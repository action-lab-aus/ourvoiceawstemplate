const AWS = require("aws-sdk");
const dbClient = new AWS.DynamoDB.DocumentClient();

exports.getUsers = function (event, callback) {
  console.log("getUsers", JSON.stringify(event));

  var params = {
    TableName: "OurVoiceUser-Test",
    // Set maximum number of items to be read
    Limit: event.parameters.limit || 10,
  };

  if (event.parameters.next) {
    // Define the primary key of the first item that this operation will evaluate
    params.ExclusiveStartKey = { userId: event.parameters.next };
  }

  // db.scan will return one or more items by accessing every item in a table or a secondary index
  dbClient.scan(params, function (err, data) {
    if (err) {
      callback(new Error(`Error: ${err.message}`));
    } else {
      var res = { body: data.Items };
      if (data.LastEvaluatedKey !== undefined) {
        res.headers = { next: data.LastEvaluatedKey.userId };
      }
      callback(null, res);
    }
  });
};

exports.postUser = function (event, callback) {
  console.log("postUser", JSON.stringify(event));

  var params = {
    // Define the item to be posted
    Item: {
      userId: event.body.userId,
      regDate: event.body.regDate,
      email: event.body.email,
      name: event.body.name,
    },
    TableName: "OurVoiceUser-Test",
    // Ensure no duplicate user will be posted
    ConditionExpression: "attribute_not_exists(email)",
  };

  dbClient.put(params, function (err) {
    if (err) {
      callback(new Error(`Error: ${err.message}`));
    } else {
      callback(null, {
        headers: { userId: event.body.userId },
        body: params.Item,
      });
    }
  });
};

exports.verifyUser = function (event, callback) {
  console.log("verifyUser", JSON.stringify(event));

  var params = {
    Key: { email: event.parameters.email },
    TableName: "OurVoiceWhitelist-Test",
  };

  dbClient.get(params, function (err, data) {
    if (err) {
      callback(new Error(`Error: ${err.message}`));
    } else {
      if (data.Item) {
        callback(null, { body: data.Item });
      } else {
        callback(new Error("Error: User not found"));
      }
    }
  });
};

exports.getUser = function (event, callback) {
  console.log("getUser", JSON.stringify(event));

  var params = {
    Key: { userId: event.parameters.userId },
    TableName: "OurVoiceUser-Test",
  };

  dbClient.get(params, function (err, data) {
    if (err) {
      callback(new Error(`Error: ${err.message}`));
    } else {
      if (data.Item) {
        callback(null, { body: data.Item });
      } else {
        callback(new Error("Error: User not found"));
      }
    }
  });
};

exports.deleteUser = function (event, callback) {
  console.log("deleteUser", JSON.stringify(event));

  var params = {
    Key: { userId: event.parameters.userId },
    TableName: "OurVoiceUser-Test",
    ReturnValues: "ALL_OLD",
    ConditionExpression: "attribute_exists(userId)",
  };

  dbClient.delete(params, function (err, data) {
    if (err) {
      callback(new Error(`Error: ${err.message}`));
    } else {
      callback(null, { body: data.Attributes });
    }
  });
};

exports.getPosts = function (event, callback) {
  console.log("getPosts", JSON.stringify(event));

  var params = {
    TableName: "OurVoicePost-Test",
    Limit: event.parameters.limit || 10,
  };

  if (event.parameters.next) {
    params.ExclusiveStartKey = { postId: event.parameters.next };
  }

  dbClient.scan(params, function (err, data) {
    if (err) {
      callback(new Error(`Error: ${err.message}`));
    } else {
      var res = { body: data.Items };
      if (data.LastEvaluatedKey !== undefined) {
        res.headers = { next: data.LastEvaluatedKey.postId };
      }
      callback(null, res);
    }
  });
};

exports.postPost = function (event, callback) {
  console.log("postPost", JSON.stringify(event));

  var countParams = {
    TableName: "OurVoicePost-Test",
    Select: "COUNT",
  };

  dbClient.scan(countParams, function (err, countData) {
    if (err) {
      callback(new Error(`Error: ${err.message}`));
    } else {
      const count = countData.Count + 1;
      const postId = `P-${("000" + count).substr(-4)}`;
      var params = {
        Item: {
          postId: postId,
          postDate: event.body.postDate,
          category: event.body.category,
          postedBy: event.body.postedBy,
          content: event.body.content,
          comments: [],
          moderations: [],
        },
        TableName: "OurVoicePost-Test",
        ConditionExpression: "attribute_not_exists(postId)",
      };

      dbClient.put(params, function (err) {
        if (err) {
          callback(new Error(`Error: ${err.message}`));
        } else {
          const reviewParams = {
            Item: {
              postId: postId,
              commId: "null",
              reviewNo: 0,
              moderatedBy: [],
            },
            TableName: "OurVoiceReview-Test",
          };

          dbClient.put(reviewParams, function (err) {
            if (err) {
              callback(new Error(`Error: ${err.message}`));
            } else {
              callback(null, {
                headers: { postId: postId },
                body: params.Item,
              });
            }
          });
        }
      });
    }
  });
};

exports.getPost = function (event, callback) {
  console.log("getPost", JSON.stringify(event));

  var params = {
    Key: { postId: event.parameters.postId },
    TableName: "OurVoicePost-Test",
  };

  dbClient.get(params, function (err, data) {
    if (err) {
      callback(new Error(`Error: ${err.message}`));
    } else {
      if (data.Item) {
        callback(null, { body: data.Item });
      } else {
        callback(new Error("Error: Post not found"));
      }
    }
  });
};

exports.updatePost = function (event, callback) {
  console.log("updatePost", JSON.stringify(event));

  const updateType = event.parameters.updateType;
  var params;
  if (updateType === "postComment") {
    params = {
      Key: { postId: event.parameters.postId },
      TableName: "OurVoicePost-Test",
      ReturnValues: "UPDATED_NEW",
      UpdateExpression: "SET #comm = list_append(#comm, :newComm)",
      ExpressionAttributeNames: { "#comm": "comments" },
      ExpressionAttributeValues: {
        ":newComm": [
          {
            commId: event.body.commId,
            commDate: event.body.commDate,
            commBy: event.body.commBy,
            commContent: event.body.commContent,
            moderations: [],
          },
        ],
      },
    };
  } else if (updateType === "updateFacebookId") {
    params = {
      Key: { postId: event.parameters.postId },
      TableName: "OurVoicePost-Test",
      ReturnValues: "UPDATED_NEW",
      UpdateExpression: "SET #newAttr = :newAttr",
      ExpressionAttributeNames: { "#newAttr": "facebookId" },
      ExpressionAttributeValues: {
        ":newAttr": event.body.facebookId,
      },
    };
  }

  dbClient.update(params, function (err, data) {
    if (err) {
      callback(new Error(`Error: ${err.message}`));
    } else {
      if (updateType === "postComment") {
        const reviewParams = {
          Item: {
            postId: event.parameters.postId,
            commId: event.body.commId,
            reviewNo: 0,
            moderatedBy: [],
          },
          TableName: "OurVoiceReview-Test",
        };

        dbClient.put(reviewParams, function (err) {
          if (err) {
            callback(new Error(`Error: ${err.message}`));
          } else {
            callback(null, { body: data.Attributes });
          }
        });
      }
    }
  });
};

exports.deletePost = function (event, callback) {
  console.log("deletePost", JSON.stringify(event));

  var params = {
    Key: { postId: event.parameters.postId },
    TableName: "OurVoicePost-Test",
    ReturnValues: "ALL_OLD",
    ConditionExpression: "attribute_exists(postId)",
  };

  dbClient.delete(params, function (err, data) {
    if (err) {
      callback(new Error(`Error: ${err.message}`));
    } else {
      var reviewParams = {
        TableName: "OurVoiceReview-Test",
        Key: { postId: event.parameters.postId },
        ConditionExpression: "attribute_exists(postId)",
      };

      dbClient.delete(reviewParams, function (err) {
        if (err) {
          callback(new Error(`Error: ${err.message}`));
        } else {
          callback(null, { body: data.Attributes });
        }
      });
    }
  });
};

exports.getReviews = function (event, callback) {
  console.log("getReviews", JSON.stringify(event));

  var reviewParams = { TableName: "OurVoiceReview-Test" };

  dbClient.scan(reviewParams, function (err, reviewData) {
    if (err) {
      callback(new Error(`Error: ${err.message}`));
    } else {
      const count = reviewData.Items.length;
      callback(null, {
        headers: { count: count },
        body: reviewData.Items,
      });
    }
  });
};

exports.deleteReview = function (event, callback) {
  console.log("deleteReview", JSON.stringify(event));

  const postId = event.parameters.postId;
  const commId = event.parameters.commId;

  var reviewParams = {
    TableName: "OurVoiceReview-Test",
    Key: { postId: postId, commId: commId ? commId : "null" },
  };

  // Delete the review item
  dbClient.delete(reviewParams, function (err, data) {
    if (err) {
      callback(new Error(`Error: ${err.message}`));
    } else {
      callback(null, { body: data.Attributes });
    }
  });
};

exports.postReview = function (event, callback) {
  console.log("postReview", JSON.stringify(event));

  const postId = event.parameters.postId;
  const commId = event.parameters.commId;
  const commIndex = event.parameters.commIndex;

  let params;
  if (!commId)
    params = {
      Key: { postId: postId },
      TableName: "OurVoicePost-Test",
      ReturnValues: "ALL_NEW",
      ConditionExpression: "attribute_exists(postId)",
      UpdateExpression: "SET #moder = list_append(#moder, :newModer)",
      ExpressionAttributeNames: { "#moder": "moderations" },
      ExpressionAttributeValues: {
        ":newModer": [
          {
            moderator: event.body.moderator,
            moderDate: event.body.moderDate,
            content: event.body.content,
          },
        ],
      },
    };
  else
    params = {
      Key: { postId: postId },
      TableName: "OurVoicePost-Test",
      ReturnValues: "ALL_NEW",
      ConditionExpression: "attribute_exists(postId)",
      UpdateExpression:
        "SET comments[" +
        parseInt(commIndex) +
        "].#moder = list_append(comments[" +
        parseInt(commIndex) +
        "].#moder, :newModer)",
      ExpressionAttributeNames: {
        "#moder": "moderations",
      },
      ExpressionAttributeValues: {
        ":newModer": [
          {
            moderator: event.body.moderator,
            moderDate: event.body.moderDate,
            content: event.body.content,
          },
        ],
      },
    };

  dbClient.update(params, function (err, data) {
    if (err) {
      callback(new Error(`Error: ${err.message}`));
    } else {
      var reviewParams = {
        TableName: "OurVoiceReview-Test",
        Key: { postId: postId, commId: commId ? commId : "null" },
      };

      dbClient.get(reviewParams, function (err, reviewData) {
        if (err) {
          callback(new Error(`Error: ${err.message}`));
        } else {
          if (reviewData.Item) {
            // Update the reviewNo
            reviewParams.UpdateExpression =
              "set reviewNo = reviewNo + :val, #moder = list_append(#moder, :newModer)";
            reviewParams.ExpressionAttributeNames = {
              "#moder": "moderatedBy",
            };
            reviewParams.ExpressionAttributeValues = {
              ":val": 1,
              ":newModer": [event.body.moderator],
            };
            reviewParams.ReturnValues = "UPDATED_NEW";

            dbClient.update(reviewParams, function (err) {
              if (err) {
                callback(new Error(`Error: ${err.message}`));
              } else {
                callback(null, { body: data.Attributes });
              }
            });
          } else {
            callback(new Error(`Error: Review not found`));
          }
        }
      });
    }
  });
};
