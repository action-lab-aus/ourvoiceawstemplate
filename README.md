# AWS CloudFormation - Serverless Template

## 1. Export Default AWS Region

`export AWS_DEFAULT_REGION=ap-southeast-2`

## 2. Initialzie S3 Bucket

`export S3Bucket=ourvoice-apigateway`
`aws s3 mb s3://$S3Bucket`
`aws s3 cp ourvoice-lambda.zip s3://$S3Bucket/ourvoice-lambda.zip`

## 3. Create CloudFormation Stack Using the Template

`aws cloudformation create-stack --stack-name ourvoice-apigateway --template-body file://template.json --capabilities CAPABILITY_IAM --parameters ParameterKey=S3Bucket,ParameterValue=$S3Bucket`

## 4. Check CloudFormation Stack Status (via Console/CLI)

`aws cloudformation wait stack-create-complete --stack-name ourvoice-apigateway`

## 5. Get the ApiId for the ApiGateway

`aws cloudformation describe-stacks --stack-name ourvoice-apigateway --query Stacks[0].Outputs`

## 6. Store the ApiId Retrieve and Store in the URL String

`export OurVoiceApiGatewayEndpoint="$ApiId.execute-api.ap-southeast-2.amazonaws.com/v1"`
