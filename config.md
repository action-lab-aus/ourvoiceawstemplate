# AWS::DynamoDB::Table

## AttributeDefinitions

A list of attributes that describe the key schema for the table and indexes

## KeySchema

Specifies the attributes that make up the primary key for the table. The attributes in the KeySchema property must also be defined in the AttributeDefinitions property

HASH - Partition Key
RANGE - Sort Key

## ProvisionedThroughput

Throughput for the specified table, which consists of values for ReadCapacityUnits and WriteCapacityUnits

Read Capacity Unit - represents one strongly consistent read per second, or two eventually consistent reads per second, for an item up to 4 KB in size **\$0.10656 per RCU per Month**

Write Capacity Unit - represents one write per second, for an item up to 1 KB in size **\$0.5328 per WCU per Month**

## BillingMode

Specify how you are charged for read and write throughput and how you manage capacity

PROVISIONED - use PROVISIONED for predictable workloads
PAY_PER_REQUEST - use PAY_PER_REQUEST for unpredictable workloads **$0.285 per Million Read Request, $1.4231 per Million Write Request** (for lower amount of requests, PAY_PER_REQUEST will be cheaper)

# AWS::IAM::Role

Assign Lambda Function with the Role being abel to perform actions on DynamoDB and Logs

## AssumeRolePolicyDocument

The trust policy that is associated with this role. Trust policies define which entities can assume the role. You can associate only one trust policy with a role

## Policies

Adds or updates an inline policy document that is embedded in the specified IAM role. When you embed an inline policy in a role, the inline policy is used as part of the role's access (permissions) policy. The role's trust policy is created at the same time as the role. You can update a role's trust policy later

# AWS::Lambda::Function

Creates a Lambda function for the ApiGateway

## Code

The deployment package for a Lambda function. For all runtimes, you can specify the location of an object in Amazon S3. For Node.js and Python functions, you can specify the function code inline in the template with a limitation of 4096 characters

## Environment

Environment variables that are accessible from function code during execution (nodejs12.x)

## Handler

The name of the method within your code that Lambda calls to execute your function. The format includes the file name. It can also include namespaces and other qualifiers, depending on the runtime

## MemorySize

The amount of memory that your function has access to. Increasing the function's memory also increases its CPU allocation. The default value is 128 MB. The value must be a multiple of 64 MB **Currently: 256 MB**

## Timeout

The amount of time that Lambda allows a function to run before stopping it. The default is 3 seconds. The maximum allowed value is 900 seconds **Currently: 10 Seconds**

# AWS::Lambda::Permission

Grants an AWS service or another account permission to use a function. You can apply the policy at the function level, or specify a qualifier to restrict access to a single version or alias. If you use a qualifier, the invoker must use the full Amazon Resource Name (ARN) of that version or alias to invoke the function

## Action

The action that the principal can use on the function. For example, lambda:InvokeFunction or lambda:GetFunction

## FunctionName

The name of the Lambda function, version, or alias, name formats:
_Function Name_ - my-function (name-only), my-function:v1 (with alias)
_Function ARN_ - arn:aws:lambda:us-west-2:123456789012:function:my-function
_Partial ARN_ - 123456789012:function:my-function

## Principal

The AWS service or account that invokes the function. If you specify a service, use SourceArn or SourceAccount to limit who can invoke the function through that service **Use ApiGateway to Invoke the Function**

## SourceArn

For AWS services, the ARN of the AWS resource that invokes the function. For example, an Amazon S3 bucket or Amazon SNS topic

# AWS::ApiGateway::RestApi

Creates a REST API using AWS ApiGateway. We don't need to specify detail within this resource. However, both the resources, models, and methods for ApiGateway will be referencing the resource for provisioning the infrastructure

## EndpointConfiguration

A list of the endpoint types of the API. Use this property when creating an API. When importing an existing API, specify the endpoint configuration types using the Parameters property

# AWS::ApiGateway::Model

Defines the structure of a request or response payload for an API method. For each DynamoDb Table, create e.g. UserModel, UsersModel, CreateUserModel for restricting the payloads

## ContentType

The content type for the model

## RestApiId

The ID of a REST API with which to associate this model

## Schema

The schema to use to transform data to one or more output formats. Specify null ({}) if you don't want to specify a schema

# AWS::ApiGateway::Resource

Creates a resource in an API

## RestApiId

The ID of the RestApi resource in which API Gateway creates the method

## ParentId

If you want to create a child resource, the ID of the parent resource. For resources without a parent, specify the RestApi root resource ID, such as { "Fn::GetAtt": ["MyRestApi", "RootResourceId"] }

## PathPart

A path name for the resource

# AWS::ApiGateway::Method

Creates API Gateway methods that define the parameters and body that clients must send in their requests

## RestApiId

The ID of the RestApi resource in which API Gateway creates the method

## ResourceId

The ID of an API Gateway resource. For root resource methods, specify the RestApi root resource ID, such as { "Fn::GetAtt": ["MyRestApi", "RootResourceId"] }

## HttpMethod

The HTTP method that clients use to call this method

## AuthorizationType

The method's authorization type, this parameter is required

## RequestParameters

The request parameters that API Gateway accepts. Specify request parameters as key-value pairs (string-to-Boolean mapping), with a source as the key and a Boolean as the value. The Boolean specifies whether a parameter is required. A source must match the format method.request.location.name, where the location is query string, path, or header, and name is a valid, unique parameter name

## MethodResponses

The responses that can be sent to the client who calls the method

### ResponseModels

- The resources used for the response's content type. Specify response models as key-value pairs (string-to-string maps), with a content type as the key and a Model resource name as the value

### ResponseParameters

- Response parameters that API Gateway sends to the client that called a method
- Specify response parameters as key-value pairs (string-to-Boolean maps), with a destination as the key and a Boolean as the value. Specify the destination using the following pattern: method.response.header.name, where name is a valid, unique header name. The Boolean specifies whether a parameter is required

## Integration

The backend system that the method calls when it receives a request

### Type

- The type of backend that your method is running, such as HTTP, AWS, MOCK

### Uri

- The Uniform Resource Identifier (URI) for the integration
- If you specify AWS for the Type property, specify an AWS service that follows this form: arn:aws:apigateway:region:subdomain.service|service:path|action/service_api
- For example, a Lambda function URI follows this form: arn:aws:apigateway:region:lambda:path/path. The path is usually in the form /2015-03-31/functions/LambdaFunctionARN/invocations

### IntegrationResponses

- The response that API Gateway provides after a method's backend completes processing a request
- API Gateway intercepts the response from the backend so that you can control how API Gateway surfaces backend responses
- For example, you can map the backend status codes to codes that you define
- **ResponseTemplates**: templates that are used to transform the integration response body. Specify templates as key-value pairs (string-to-string mappings), with a content type as the key and a template as the value
- e.g. `$input.json('$.body')` returns a JSON string representing the body structure returned from the Lambda Function
- **ResponseParameters**: the response parameters from the backend response that API Gateway sends to the method response

```
"ResponseParameters": {
    "method.response.header.Link": "integration.response.body.headers.next"
},
```

- e.g. the integration response header will contain a Link parameter, which will be set by the Lambda Function in the `res.headers` section

### PassthroughBehavior

- Indicates when API Gateway passes requests to the targeted backend
- This behavior depends on the request's Content-Type header and whether you defined a mapping template for it

### RequestTemplates

- A map of Apache Velocity templates that are applied on the request payload
- The template that API Gateway uses is based on the value of the Content-Type header that's sent by the client
- The content type value is the key, and the template is the value (specified as a string)
- e.g. `application/json": "{\"fun\": \"getUsers\", \"parameters\": {\"limit\": \"$input.params('limit')\", \"next\": \"$input.params('next')\"}}` will be converting the request into JSON format from client to Lambda functions because Lambda function requires its input as a JSON document

  - Example ensures that the `event` passed to Lambda function will have a `fun` paramter with value `getUsers`
  - Example also parses the request paramters into `parameters` object with `limit` and `next`

# AWS::ApiGateway::Deployment

## RestApiId

The ID of the RestApi resource to deploy

## StageName

A name for the stage that API Gateway creates with this deployment, and only alphanumeric characters are allowed
